# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: unicorn <unicorn@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/11 13:21:13 by mleroi            #+#    #+#              #
#    Updated: 2019/01/13 21:08:25 by unicorn          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ./target/debug/computorcomputor

CARGO = cargo

all	: build

build:
	@$(CARGO) build --release

clean:
	@$(CARGO) clean

fclean : clean
	rm -f $(NAME)

run: build
	@$(CARGO) run

re : fclean all