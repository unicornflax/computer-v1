/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.rs                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mleroi <mleroi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 14:59:19 by mleroi            #+#    #+#             */
/*   Updated: 2019/01/25 11:19:15 by mleroi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

extern crate itertools;

extern crate clap;
use clap::{App, Arg};

#[macro_use]
extern crate nom;
use nom::types::CompleteStr;

mod calcul;
use crate::calcul::solve;

mod parser;
use crate::parser::equation_parser;

mod resolver;
use crate::resolver::{calcul_reduce_form, PrettyPrint, SimplifyEquation};

fn main() {
    let matches = App::new("computor")
        .version("0.1.0")
        .author("mleroi")
        .about("My computor V1 in rust")
        .arg(
            Arg::with_name("equation")
                .required(true)
                .help("set the polynomiale equation to solve"),
        )
        .get_matches();
    let brut_equation = matches.value_of("equation").unwrap();
    let equation = equation_parser(CompleteStr(brut_equation));
    match equation {
        Ok((s, eq)) => {
            if s.is_empty() {
                match calcul_reduce_form(eq) {
                    Ok(eq) => {
                        let reduce_form: SimplifyEquation = eq;
                        reduce_form.pretty_print();
                        solve(reduce_form);
                    }
                    Err(e) => println!("{}", e),
                };
            } else {
                println!("Invalid Input there {}", s);
            }
        }
        Err(err) => println!("Invalid Input : {}", err),
    };
}
