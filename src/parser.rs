/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.rs                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mleroi <mleroi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 00:09:41 by unicorn           #+#    #+#             */
/*   Updated: 2019/01/22 15:32:56 by mleroi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//grammar
// <equation> ::= <polynome> "=" <polynome>
// <polynome> ::= <nom> ["+" | "-" <nom>]*
// <nom>      ::= <number> ["*" <exposant>] | <exposant>
// <exposant> ::= "X" ["^" <number>]

use nom::{digit, types::CompleteStr};

type Exponant = u64;

#[derive(Debug, PartialEq, Clone)]
pub enum Sign {
    Negative,
    Positive,
}

impl From<char> for Sign {
    fn from(c: char) -> Self {
        match c {
            '-' => Sign::Negative,
            '+' => Sign::Positive,
            _ => panic!("Unrecognized Sign"),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Nome {
    pub number: f64,
    pub exp: Exponant,
}

#[derive(Debug, Clone)]
pub struct Polynome {
    pub polylist: Vec<(Sign, Nome)>,
}

#[derive(Debug)]
pub struct Equation {
    pub left: Polynome,
    pub right: Polynome,
}

named!(unsigned_digit<CompleteStr, u64>,
    flat_map!(
        recognize!(digit),
        parse_to!(u64)
        )
    );

named!(exponant_parser<CompleteStr, Exponant>,
    ws!(do_parse!(
        tag_no_case!("x") >>
        myexponant : opt!(preceded!(tag!("^"), unsigned_digit)) >>
        (myexponant.unwrap_or(1))
    ))
);

named!(nome_parser<CompleteStr, Nome>,
    alt!(
        ws!(do_parse!(
            number : call!(nom::double) >>
            exp : opt!(preceded!(tag!("*"), exponant_parser)) >>
            (Nome{number : number, exp : exp.unwrap_or(0)})
            ))
        |
        do_parse!(
            exp : exponant_parser >>
            (Nome{number : 1.0, exp : exp})
        )
    )
);

named!(pub(crate) polynome_parser<CompleteStr, Polynome>,
    do_parse!(
        first : nome_parser >>
        vec_nome : many0!(
            tuple!(
                map!(alt!(char!('+') | char!('-')), Sign::from),
                nome_parser
            )) >>
        (create_vector_of_tuple(first, vec_nome))
    )
);

named!(pub(crate) equation_parser<CompleteStr, Equation>,
    ws!(
        do_parse!(
        pair : separated_pair!(
            polynome_parser,
            char!('='),
            polynome_parser
        ) >>
        (Equation{left : pair.0, right : pair.1})
        ))
);

fn create_vector_of_tuple(head: Nome, tail: Vec<(Sign, Nome)>) -> Polynome {
    let mut v = Vec::new();
    let mut t = tail;
    v.push((Sign::Positive, head));
    v.append(&mut t);
    let p = Polynome { polylist: v };
    p
}
