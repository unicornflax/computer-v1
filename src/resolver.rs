/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolver.rs                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mleroi <mleroi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 18:04:38 by mleroi            #+#    #+#             */
/*   Updated: 2019/01/25 17:09:25 by mleroi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

use crate::parser::{Equation, Nome, Sign};
use itertools::Itertools;

#[derive(Debug)]
pub struct SimplifyEquation {
    pub left: Poly,
    pub right: Poly,
    pub degre: u64,
}

#[derive(Debug)]
pub struct Poly {
    pub polylist: Vec<Nome>,
}

type NoSignEq = SimplifyEquation;

type ZeroRightEq = SimplifyEquation;

type SortedEquation = SimplifyEquation;

pub trait PrettyPrint {
    fn pretty_print(&self) -> ();
}

impl PrettyPrint for SimplifyEquation {
    fn pretty_print(&self) {
        let (first, second) = self.left.polylist.split_first().unwrap();
        let s2 = format!("Reduced form: ");
        let mut s = match first.exp {
            0 if first.number != 0.0 => format!("{}", first.number),
            1 if first.number != 0.0 => format!("{} * X", first.number),
            _ if first.number != 0.0 => format!("{} * X^{}", first.number, first.exp),
            _ => format!(""),
        };
        for nome in second {
            let str_nb = if nome.number < 0.0 {
                match nome.exp {
                    1 if nome.number != 0.0 && s == "" => format!("{} * X", nome.number),
                    1 if nome.number != 0.0 => format!(" - {} * X", change_sign_nom(nome).number),
                    _ if nome.number != 0.0 && s == "" => format!("{} * X^{}", nome.number, nome.exp),
                    _ if nome.number != 0.0 => {
                        format!(" - {} * X^{}", change_sign_nom(nome).number, nome.exp)
                    }
                    _ => format!(""),
                }
            } else {
                match nome.exp {
                    1 if nome.number != 0.0 && s == "" => format!("{} * X", nome.number),
                    1 if nome.number != 0.0 => format!(" + {} * X", nome.number),
                    _ if nome.number != 0.0 && s == "" => format!("{} * X^{}", nome.number, nome.exp),
                    _ if nome.number != 0.0 => format!(" + {} * X^{}", nome.number, nome.exp),
                    _ => format!(""),
                }
            };
            s = s + &*str_nb
        }
        if s == "" {
            s = format!("0");
        }
        println!("{}{} = 0", s2, s);
    }
}

fn polynome_is_empty(poly: &Vec<(Nome)>) -> bool {
    for nome in poly {
        if nome.number != 0.0 || nome.exp != 0 {
            return false;
        }
    }
    true
}

pub fn change_sign_nom(n: &Nome) -> Nome {
    Nome {
        exp: n.exp,
        number: n.number * -1.0,
    }
}

fn supress_sign(equation: Equation) -> NoSignEq {
    let mut n_eq = NoSignEq {
        left: Poly {
            polylist: Vec::new(),
        },
        right: Poly {
            polylist: Vec::new(),
        },
        degre: 0,
    };
    for v in equation.left.polylist {
        let tup = match v {
            (Sign::Positive, _) => (v.1),
            (Sign::Negative, nome) => (change_sign_nom(&nome)),
        };
        n_eq.left.polylist.push(tup);
    }
    for v in equation.right.polylist {
        let tup = match v {
            (Sign::Positive, nome) => (nome),
            (Sign::Negative, nome) => (change_sign_nom(&nome)),
        };
        n_eq.right.polylist.push(tup);
    }
    n_eq
}

fn move_to_left(equation: NoSignEq) -> ZeroRightEq {
    let mut n_eq = ZeroRightEq {
        left: Poly {
            polylist: equation.left.polylist,
        },
        right: Poly {
            polylist: vec![Nome {
                number: 0.0,
                exp: 0,
            }],
        },
        degre: 0,
    };
    n_eq.left.polylist.append(
        &mut equation
            .right
            .polylist
            .iter()
            .map(|t| match t {
                nome if nome.number >= 0.0 => change_sign_nom(nome),
                nome if nome.number < 0.0 => change_sign_nom(nome),
                _ => panic!(),
            })
            .collect::<Vec<Nome>>(),
    );
    n_eq
}

fn sort_polynome(mut eq: ZeroRightEq) -> SortedEquation {
    eq.left.polylist.sort_unstable_by(|a, b| a.exp.cmp(&b.exp));
    eq
}

fn simplify_exponant(eq: SortedEquation) -> Result<SimplifyEquation, &'static str> {
    let mut n_eq = SimplifyEquation {
        left: Poly {
            polylist: Vec::new(),
        },
        right: Poly {
            polylist: vec![Nome {
                number: 0.0,
                exp: 0,
            }],
        },
        degre: eq.degre,
    };
    match eq.left.polylist.last() {
        Some(value) => {
            n_eq.degre = value.exp;
            let max_exponant = value.exp;
            let mut iterator = eq.left.polylist.into_iter();
            for i in 0..=max_exponant {
                let nb = iterator
                    .take_while_ref(|x| x.exp == i)
                    .fold(0.0, |acc, x| acc + x.number);
                if nb != 0.0 {
                    n_eq.degre = i;
                };
                n_eq.left.polylist.push(Nome { number: nb, exp: i });
            }
            Ok(n_eq)
        }
        None => Err("should not happened"),
    }
}

pub fn calcul_reduce_form(equation: Equation) -> Result<SimplifyEquation, &'static str> {
    let equation = supress_sign(equation);
    if polynome_is_empty(&equation.right.polylist) == false {
        let equation = move_to_left(equation);
        let equation = sort_polynome(equation);
        let equation = simplify_exponant(equation);
        equation
    } else {
        let equation = sort_polynome(equation);
        let equation = simplify_exponant(equation);
        equation
    }
}
