/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calcul.rs                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mleroi <mleroi@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 16:47:27 by mleroi            #+#    #+#             */
/*   Updated: 2019/01/25 17:04:36 by mleroi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

use crate::parser::Nome;
use crate::resolver::SimplifyEquation;

pub struct Complex {
    pub re: f64,
    pub im: f64,
}

type Imaginary = f64;

fn my_power(number: f64, power: u64) -> f64 {
    match power {
        power if power > 1 => {
            let n = number * my_power(number, power - 1);
            n
        }
        _ => number,
    }
}

fn complex_sqrt(nb: f64) -> Imaginary {
    sqrt(nb * -1.0).unwrap()
}

fn sqrt(nb: f64) -> Option<f64> {
    if nb == 1.0 {
        Some(1.0)
    } else {
        let mut i = 1.0;
        while nb > i * i {
            i += 0.000001;
        }
        Some(i)
    }
}

fn one_soluce(a: Nome, b: Nome, c: Nome) -> () {
    let result = (b.number * -1.0) / (2.0 * a.number) + (c.number * 1.0) - c.number;
    if result.is_nan() {
        println!("All values of x are solutions");
    } else {
        println!("Discriminant is zero, the solution is:\n{}", result);
    }
}

fn two_complexe_soluce(a: Nome, b: Nome, c: Nome, result: f64) -> () {
    let sqrt = complex_sqrt(result);
    let mut result_one = Complex { re: 0.0, im: 0.0 };
    let mut result_two = Complex { re: 0.0, im: 0.0 };
    result_one.re = (b.number * -1.0) / (2.0 * a.number) + (c.number * 1.0) - c.number;
    result_one.im = (sqrt * -1.0) / (2.0 * a.number) + (c.number * 1.0) - c.number;
    result_two.re = (b.number * -1.0) / (2.0 * a.number) + (c.number * 1.0) - c.number;
    result_two.im = sqrt / (2.0 * a.number) + (c.number * 1.0) - c.number;
    println!(
        "Discriminant is negative, the two solutions are:\n{}r {}i\n{}r {}i",
        result_one.re, result_one.im, result_two.re, result_two.im
    );
}
fn two_simple_soluce(a: Nome, b: Nome, c: Nome, result: f64) -> () {
    let result_one = ((b.number * -1.0) - sqrt(result).unwrap()) / (2.0 * a.number)
        + (c.number * 1.0)
        - c.number;
    let result_two = ((b.number * -1.0) + sqrt(result).unwrap()) / (2.0 * a.number)
        + (c.number * 1.0)
        - c.number;
    println!(
        "Discriminant is strictly positive, the two solutions are:\n{}\n{}",
        result_one, result_two
    );
}

fn calcul_discriminant(equation: SimplifyEquation) -> () {
    let c = equation.left.polylist[0].clone();
    let b = equation.left.polylist[1].clone();
    let a = equation.left.polylist[2].clone();
    let result = my_power(b.number, 2) - 4.0 * a.number * c.number;
    match result {
        _ if result > 0.0 => two_simple_soluce(a, b, c, result),
        _ if result < 0.0 => two_complexe_soluce(a, b, c, result),
        _ => one_soluce(a, b, c),
    }
}

fn first_degree(equation: SimplifyEquation) -> f64 {
    let c = equation.left.polylist[0].clone();
    let b = equation.left.polylist[1].clone();
    let result = (c.number * -1.0) / b.number;
    result
}

fn print_soluce(res: f64) -> () {
    if res.is_nan() {
        println!("All values of x are solutions");
    } else {
        println!("The solution is:\n{}", res);
    }
}

pub fn solve(equation: SimplifyEquation) {
    match equation.degre {
        0 => {
            println!("Polynomial degree: {}", 0);
            if equation.left.polylist[0].number == 0.0 && equation.left.polylist[0].exp == 0 {
                println!("All values of x are solutions");
            } else {
                println!("No solutions exist");
            }
        }
        1 => {
            println!("Polynomial degree: {}", 1);
            let result = first_degree(equation);
            print_soluce(result);
        }
        2 => {
            println!("Polynomial degree: {}", 2);
            calcul_discriminant(equation)
        }
        x => {
            println!("Polynomial degree: {}", x);
            println!("The polynomial degree is stricly greater than 2, I can't solve");
        }
    }
}
